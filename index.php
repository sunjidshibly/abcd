<html>
    <head>
        <meta charset="UTF-8">
        <title>Database connection</title>
        <style>
           *{margin: 0px auto;
           padding-top: 10px;
           }  
        </style>
    </head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <body>
        
          <?php
        include_once 'book.php';
        $list = new Book();
        $book_array = $list->index();
        ?>
        <table style="width: 60%" border="1">
            <thead>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
        </thead>
        <tr>
            <?php foreach ($book_array as $Book): ?>
            <td> <?php echo $Book['id']; ?></td>
            <td> <?php echo $Book['title']; ?></td>
            <td> <?php echo $Book['author']; ?></td>
        </tr>  

        <?php    endforeach; ?>
    </table>   
        
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>